### Noms des binomes : OUWAB ELIDRISSI , ELACHIQI
### étape 1 : On fournit à terraform nos variables d'environement sur Openstack en exécutant la commande suivante :
 source elmehdi.ouwabelidrissi.etu-openrc.sh

### étape 2 : On crée un fichiers providers.tf où on va mettre toutes les resources nécessaires afin de créer nos instances sur openstack

### étape 3 : On initialise terraform
 terraform init

### étape 4 : Mis à jour de terraform
terraform {
required_version = ">= 0.13.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.35.0"
    }
  }
}

### étape 5 : On s'authentifie sur openstack en configurant la clé ssh
```
resource "openstack_compute_keypair_v2" "keypair" {
  provider = openstack
  name = "ssh-key"
  public_key  = file("~/.ssh/id_rsa.pub")
}
```
### étape 5 : je crée deux instances portant les noms (terraform_instance_ub-1 , terraform_instance_ub-0) sous Ubuntu
```
resource "openstack_compute_instance_v2" "terraform_instances_ub" {
  count = 2
  name = "terraform_instance_ub-${count.index}"
  provider = openstack
  image_name = "ubuntu-20.04"
  flavor_name = "normale"
  key_pair = openstack_compute_keypair_v2.keypair.name
}
```
### étape 6 : Je crée deux autres instances portant les noms (terraform_instance_ce-0 , terraform_instance_ce-1) sous Centos
```
 resource "openstack_compute_instance_v2" "terraform_instances_ce" {
  count = 2
  name = "terraform_instance_ce-${count.index}"
  provider = openstack
  image_name = "centos-7"
  flavor_name = "normale"
  key_pair = openstack_compute_keypair_v2.keypair.name
 }
```

### étape 7 : On excécute la commande terraform apply pour la mise en place
